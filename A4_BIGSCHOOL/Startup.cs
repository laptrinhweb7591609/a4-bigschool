﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(A4_BIGSCHOOL.Startup))]
namespace A4_BIGSCHOOL
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
